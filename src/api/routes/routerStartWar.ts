import { Request, Response, NextFunction, Router } from 'express'
import StartWarController from '../../controllers/StartWarController'
import { Container } from "typescript-ioc";

export default class RouterStartWar {
  public app: Router
  constructor(router: Router) {
    this.app = router
  }
  router(): void {
    this.app.get(
      '/obtener/listado/peliculas/',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const startWarController: StartWarController = Container.get(StartWarController);
          let responseModel = await startWarController.obtenerListadoPeliculas();
          res.status(200).json(responseModel);
        } catch (error) {
          console.log(error)
        }
      }
    )
  }
}