import { Request, Response, NextFunction } from 'express'
import { Inject } from "typescript-ioc";
import { ServiosStartWar } from '../DAO/ServiosStartWar'
import requests from 'request-promise'

export default class StartWarController {
	constructor(
		@Inject private serviosStartWar: ServiosStartWar,
	) {
	}
	async obtenerListadoPeliculas(): Promise<void> {
		let res:any;
		let resName:[]|any=[]
		let resPlanet:[]|any=[]
		let resPeople:[]|any=[]
		let resStarships:[]|any=[]
		try {

			let responsePeliculas:any = await this.serviosStartWar.obtenerPeliculas('https://swapi.dev/api/films/')
			for(let x in responsePeliculas.results){
				resName.push(responsePeliculas.results[x].title)
			}

			let responsePlanest:any = await this.serviosStartWar.obtenerPeliculas('https://swapi.dev/api/planets/')
		  for(let x in responsePlanest.results){
			 	resPlanet.push(
				 	{ name:  responsePlanest.results[x].name,
				 		terrain:  responsePlanest.results[x].terrain,
				 		gravity:  responsePlanest.results[x].gravity,
				 		diameter:  responsePlanest.results[x].diameter,
				 		population:  responsePlanest.results[x].population
				 	})
			}

			let responsePeople:any = await this.serviosStartWar.obtenerPeliculas('https://swapi.dev/api/people/')
			for(let x in responsePeople.results){
			 	resPeople.push(
				 	{ name:  responsePeople.results[x].name,
				 		gender:  responsePeople.results[x].gender,
				 		hair_color:  responsePeople.results[x].hair_color,
				 		skin_color:  responsePeople.results[x].skin_color,
				 		eye_color:  responsePeople.results[x].eye_color,
				 		height:  responsePeople.results[x].height,
				 		mass:  responsePeople.results[x].mass
				 	})
			}

			let responseStarships:any = await this.serviosStartWar.obtenerPeliculas('https://swapi.dev/api/starships/')
			for(let x in responseStarships.results){
			 	resStarships.push(
				 	{ name:  responseStarships.results[x].name,
				 		model:  responseStarships.results[x].model,
				 		manufacturer:  responseStarships.results[x].manufacturer,
				 		passengers:  responseStarships.results[x].passengers
				 	})
			}
			

			res = {
				peliculas : resName,
				planetas : resPlanet,
				actores: resPeople,
				info_naves : resStarships
			}
			return res
		}catch(error){
			console.log(error)
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}

}
